﻿#pragma checksum "..\..\..\View\POSWindow.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "937F9E093F83142279C59E9879C4780AE1C8ACF8F9669687E3496EA7BFFEE940"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using SistemPOS.View;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SistemPOS.View {
    
    
    /// <summary>
    /// POSWindow
    /// </summary>
    public partial class POSWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 45 "..\..\..\View\POSWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox OrderCustomerName;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\..\View\POSWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl CartListView;
        
        #line default
        #line hidden
        
        
        #line 155 "..\..\..\View\POSWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox OrderGrandTotal;
        
        #line default
        #line hidden
        
        
        #line 182 "..\..\..\View\POSWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox OrderPayment;
        
        #line default
        #line hidden
        
        
        #line 210 "..\..\..\View\POSWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox OrderChange;
        
        #line default
        #line hidden
        
        
        #line 244 "..\..\..\View\POSWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button POSFoodButton;
        
        #line default
        #line hidden
        
        
        #line 245 "..\..\..\View\POSWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button POSBeverageButton;
        
        #line default
        #line hidden
        
        
        #line 249 "..\..\..\View\POSWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LoggedInFullname;
        
        #line default
        #line hidden
        
        
        #line 266 "..\..\..\View\POSWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox ProductSearch;
        
        #line default
        #line hidden
        
        
        #line 282 "..\..\..\View\POSWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl ProductListView;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SistemPOS;component/view/poswindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\View\POSWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.OrderCustomerName = ((System.Windows.Controls.TextBox)(target));
            
            #line 50 "..\..\..\View\POSWindow.xaml"
            this.OrderCustomerName.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.OrderCustomerName_TextChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.CartListView = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 5:
            this.OrderGrandTotal = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.OrderPayment = ((System.Windows.Controls.TextBox)(target));
            
            #line 187 "..\..\..\View\POSWindow.xaml"
            this.OrderPayment.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.OrderPayment_TextChanged);
            
            #line default
            #line hidden
            
            #line 188 "..\..\..\View\POSWindow.xaml"
            this.OrderPayment.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.TextBoxNumberOnly);
            
            #line default
            #line hidden
            return;
            case 7:
            this.OrderChange = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            
            #line 225 "..\..\..\View\POSWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.OrderButton_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.POSFoodButton = ((System.Windows.Controls.Button)(target));
            
            #line 244 "..\..\..\View\POSWindow.xaml"
            this.POSFoodButton.Click += new System.Windows.RoutedEventHandler(this.ProductCategoryButton_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.POSBeverageButton = ((System.Windows.Controls.Button)(target));
            
            #line 245 "..\..\..\View\POSWindow.xaml"
            this.POSBeverageButton.Click += new System.Windows.RoutedEventHandler(this.ProductCategoryButton_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.LoggedInFullname = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            
            #line 251 "..\..\..\View\POSWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.ExitPOSButton_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.ProductSearch = ((System.Windows.Controls.TextBox)(target));
            
            #line 275 "..\..\..\View\POSWindow.xaml"
            this.ProductSearch.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.ProductSearch_TextChanged);
            
            #line default
            #line hidden
            return;
            case 14:
            this.ProductListView = ((System.Windows.Controls.ItemsControl)(target));
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 3:
            
            #line 104 "..\..\..\View\POSWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.CartItemDecreaseButton_Click);
            
            #line default
            #line hidden
            break;
            case 4:
            
            #line 112 "..\..\..\View\POSWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.CartItemIncreaseButton_Click);
            
            #line default
            #line hidden
            break;
            case 15:
            
            #line 285 "..\..\..\View\POSWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.ProductItemButton_Click);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}

