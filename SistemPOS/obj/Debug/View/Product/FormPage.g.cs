﻿#pragma checksum "..\..\..\..\View\Product\FormPage.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "32F9BF80AD7D7F1EC0CBD0B8AD645A954F18B9965BF59756A4E12DA32A8DA888"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using SistemPOS.View.Product;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SistemPOS.View.Product {
    
    
    /// <summary>
    /// FormPage
    /// </summary>
    public partial class FormPage : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 43 "..\..\..\..\View\Product\FormPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.ImageBrush ProductImage;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\..\View\Product\FormPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox ProductName;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\..\..\View\Product\FormPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox ProductPrice;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\..\..\View\Product\FormPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton FoodRadioButton;
        
        #line default
        #line hidden
        
        
        #line 102 "..\..\..\..\View\Product\FormPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton BeverageRadioButton;
        
        #line default
        #line hidden
        
        
        #line 118 "..\..\..\..\View\Product\FormPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox ProductDescription;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SistemPOS;component/view/product/formpage.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\View\Product\FormPage.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.ProductImage = ((System.Windows.Media.ImageBrush)(target));
            return;
            case 2:
            
            #line 47 "..\..\..\..\View\Product\FormPage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.BrowseImageButton_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.ProductName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.ProductPrice = ((System.Windows.Controls.TextBox)(target));
            
            #line 79 "..\..\..\..\View\Product\FormPage.xaml"
            this.ProductPrice.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.TextBoxNumberOnly);
            
            #line default
            #line hidden
            return;
            case 5:
            this.FoodRadioButton = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 6:
            this.BeverageRadioButton = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 7:
            this.ProductDescription = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            
            #line 127 "..\..\..\..\View\Product\FormPage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.SaveProductButton_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

