﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using SistemPOS.Model;
using SistemPOS.View;

namespace SistemPOS.Controller
{
    class POSController
    {
        ProductModel productModel;
        CartModel cartModel;
        OrderModel orderModel;
        POSWindow posWindow;
        List<CartModel> CartList;

        public POSController(POSWindow view)
        {
            this.productModel = new ProductModel();
            this.cartModel = new CartModel();
            this.orderModel = new OrderModel();
            this.posWindow = view;

            posWindow.LoggedInFullname.Content = Application.Current.Properties["LoggedInFullname"];
        }

        public void ShowProductListView(string Category = "F", string Search = "")
        {
            BrushConverter _brushConverter = new BrushConverter();
            if (Category == "F")
            {
                posWindow.POSFoodButton.SetResourceReference(Control.StyleProperty, "ProductTypeButtonLeftActive");
                posWindow.POSBeverageButton.SetResourceReference(Control.StyleProperty, "ProductTypeButtonRight");
            }
            else if (Category == "B")
            {
                posWindow.POSFoodButton.SetResourceReference(Control.StyleProperty, "ProductTypeButtonLeft");
                posWindow.POSBeverageButton.SetResourceReference(Control.StyleProperty, "ProductTypeButtonRightActive");
            }

            productModel.Category = Category;

            List<ProductModel> ProductList = productModel.GetByCategory(Search);

            posWindow.ProductListView.ItemsSource = ProductList;
        }

        public void ShowCartListView()
        {
            CartList = cartModel.GetAll();

            orderModel.GrandTotal = 0;
            if (CartList != null && CartList.Count > 0)
            {
                foreach (CartModel cartItem in CartList)
                {
                    orderModel.GrandTotal += cartItem.TotalPrice;
                }
            }

            posWindow.CartListView.ItemsSource = CartList;
            posWindow.OrderGrandTotal.Text = String.Format("Rp. {0:n0}", orderModel.GrandTotal);

            float payment = 0;
            if (posWindow.OrderPayment.Text.ToString() != "")
            {
                payment = float.Parse(posWindow.OrderPayment.Text.ToString());
            }

            UpdateCartSummary(payment);
        }

        public void AddToCart(int ProductId, int Qty = 1)
        {
            cartModel.ProductId = ProductId;

            CartModel productInCart = cartModel.GetByProductId();
            if (productInCart != null)
            {
                int newQty = productInCart.Qty + Qty;

                if (newQty <= 0)
                {
                    productInCart.Delete();
                }
                else
                {
                    productInCart.Qty = newQty;
                    productInCart.Update();
                }
            }
            else
            {
                if (Qty > 0)
                {
                    cartModel.Qty = Qty;
                    cartModel.Insert();
                }
            }

            ShowCartListView();
        }

        public void UpdateCartSummary(float payment)
        {
            orderModel.Payment = payment;
            orderModel.Change = orderModel.Payment - orderModel.GrandTotal;

            posWindow.OrderChange.Text = String.Format("Rp. {0:n0}", orderModel.Change);
        }

        public void UpdateCustomerName(string customerName)
        {
            orderModel.CustomerName = customerName;
        }

        public void CreateOrder()
        {
            string messages = "";
            
            if (CartList.Count <= 0) {
                messages = "Cart is empty!";
            }
            else if (orderModel.CustomerName == "" || orderModel.CustomerName == null)
            {
                messages = "Customer Name is required!";
            }
            else if (orderModel.Payment <= 0)
            {
                messages = "Payment is required!";
            }
            else if (orderModel.Payment < orderModel.GrandTotal)
            {
                messages = "Payment must be greater than or equal to Grand Total";
            }

            if (messages != "")
            {
                MessageBox.Show(messages, "Validation Error");
                return;
            }

            OrderModel orderData = orderModel.Insert();

            if (orderData != null)
            {
                posWindow.OrderPayment.Text = "0";
                posWindow.OrderCustomerName.Text = "";
                ShowCartListView();

                OrderDetailWindow orderDetailWindow = new OrderDetailWindow();
                orderDetailWindow.orderId = orderData.Id;
                orderDetailWindow.ShowOrderDetail();
            }
        }
    }
}
