﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using SistemPOS.Model;
using SistemPOS.View;

namespace SistemPOS.Controller
{
    class ReportController
    {
        OrderModel orderModel;

        View.Report.ListPage reportListPage;
        View.OrderDetailWindow orderDetailWindow;

        public ReportController(View.Report.ListPage view)
        {
            this.orderModel = new OrderModel();
            this.reportListPage = view;
        }

        public ReportController(OrderDetailWindow view)
        {
            this.orderModel = new OrderModel();
            this.orderDetailWindow = view;
        }

        public void ShowReportListView(string StartDate = null, string EndDate = null)
        {
            List<OrderModel> orderList = orderModel.GetAll(StartDate, EndDate);

            reportListPage.ReportListView.ItemsSource = orderList;
        }

        public void ShowReportSummary(string StartDate = null, string EndDate = null)
        {
            OrderSummary orderSummary = orderModel.GetSummary(StartDate, EndDate);

            if (orderSummary != null)
            {
                reportListPage.DataContext = orderSummary;
            }
        }

        public void ReportDetail(int orderId, OrderDetailWindow orderDetailWindow)
        {
            orderModel.Id = orderId;
            OrderModel orderData = orderModel.GetById();

            if (orderData == null)
            {
                orderDetailWindow.Close();
                MessageBox.Show("Order Not Found", "Error");
                return;
            }

            orderDetailWindow.DataContext = orderData;
            orderDetailWindow.Show();
        }
    }
}
