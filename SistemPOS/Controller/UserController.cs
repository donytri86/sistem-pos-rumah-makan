﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using SistemPOS.Model;
using SistemPOS.View;

namespace SistemPOS.Controller
{
    class UserController
    {
        UserModel userModel;

        MainWindow parentWindow;
        View.User.ListPage userListPage;
        View.User.FormPage userFormPage;

        public UserController(View.User.ListPage view)
        {
            this.userModel = new UserModel();
            this.userListPage = view;
        }

        public UserController(View.User.FormPage view)
        {
            this.userModel = new UserModel();
            this.userFormPage = view;
        }

        public void ShowUserListView(string Search = null)
        {
            List<UserModel> userList = userModel.GetAll(Search);

            userListPage.UserListView.ItemsSource = userList;
        }

        public bool Validation()
        {
            string messages = "";
            if (userFormPage.UserFullName.Text.ToString() == "")
            {
                messages = "Name is required";
            }
            else if (userFormPage.UserName.Text.ToString() == "")
            {
                messages = "Price is required";
            }
            else if (userFormPage.UserPassword.Text.ToString() == "")
            {
                messages = "Price is required";
            }

            if (messages != "")
            {
                MessageBox.Show(messages, "Validation Error");
                return false;
            }

            return true;
        }

        public void CreateUser()
        {
            parentWindow = (MainWindow)Window.GetWindow(userListPage);

            userFormPage = new View.User.FormPage();
            parentWindow.MainWindowFrame.Navigate(userFormPage);
        }

        public bool StoreUser()
        {
            if (Validation() == false)
            {
                return false;
            }

            userModel.FullName = userFormPage.UserFullName.Text;
            userModel.UserName = userFormPage.UserName.Text;
            userModel.Password = userFormPage.UserPassword.Text;

            UserModel userData = userModel.Insert();

            if (userData == null)
            {
                MessageBox.Show("Add User Failed", "Error");
                return false;
            }

            parentWindow = (MainWindow)Window.GetWindow(userFormPage);
            parentWindow.MainWindowFrame.Navigate(new View.User.ListPage());
            return true;
        }

        public void EditUser(int UserId)
        {
            userModel.Id = UserId;
            UserModel userData = userModel.GetById();

            if (userData == null)
            {
                MessageBox.Show("User Not Found", "Error");
                return;
            }

            parentWindow = (MainWindow)Window.GetWindow(userListPage);

            userFormPage = new View.User.FormPage();
            userFormPage.DataContext = userData;
            parentWindow.MainWindowFrame.Navigate(userFormPage);
        }

        public bool UpdateUser(int UserId)
        {
            userModel.Id = UserId;
            UserModel userData = userModel.GetById();

            if (userData == null)
            {
                MessageBox.Show("User Not Found", "Error");
                return false;
            }

            if (Validation() == false)
            {
                return false;
            }

            userData.FullName = userFormPage.UserFullName.Text;
            userData.UserName = userFormPage.UserName.Text;
            userData.Password = userFormPage.UserPassword.Text;
            userData = userData.Update();

            if (userData == null)
            {
                MessageBox.Show("Update User Failed", "Error");
                return false;
            }


            parentWindow = (MainWindow)Window.GetWindow(userFormPage);

            userListPage = new View.User.ListPage();
            parentWindow.MainWindowFrame.Navigate(userListPage);
            return true;
        }

        public bool DeleteUser(int UserId)
        {
            userModel.Id = UserId;
            UserModel userData = userModel.GetById();

            if (userData == null)
            {
                MessageBox.Show("User Not Found", "Error");
                return false;
            }

            MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);

            if (messageBoxResult == MessageBoxResult.Yes)
            {
                bool result = userData.Delete();

                if (result)
                {
                    ShowUserListView();
                }

                return result;
            }

            return false;
        }
    }
}
