﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Microsoft.Win32;
using SistemPOS.Model;
using SistemPOS.View;

namespace SistemPOS.Controller
{
    class ProductController
    {
        public OpenFileDialog openFileDialog;

        MainWindow parentWindow;
        ProductModel productModel;
        View.Product.ListPage productListPage;
        View.Product.FormPage productFormPage;

        public ProductController(View.Product.ListPage view)
        {
            this.productModel = new ProductModel();
            this.productListPage = view;
        }

        public ProductController(View.Product.FormPage view)
        {
            this.productModel = new ProductModel();
            this.productFormPage = view;
            openFileDialog = new OpenFileDialog();
        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public void ShowProductListView(string Category = "F", string Search = "")
        {
            BrushConverter _brushConverter = new BrushConverter();
            if (Category == "F")
            {
                productListPage.FoodCategoryButton.SetResourceReference(Control.StyleProperty, "ProductTypeButtonLeftActive");
                productListPage.BeverageCategoryButton.SetResourceReference(Control.StyleProperty, "ProductTypeButtonRight");
            }
            else if (Category == "B")
            {
                productListPage.FoodCategoryButton.SetResourceReference(Control.StyleProperty, "ProductTypeButtonLeft");
                productListPage.BeverageCategoryButton.SetResourceReference(Control.StyleProperty, "ProductTypeButtonRightActive");
            }

            productModel.Category = Category;

            List<ProductModel> ProductList = productModel.GetByCategory(Search);

            productListPage.ProductListView.ItemsSource = ProductList;
        }

        public string CopyFileDialog(string defaultFilename = "Food-1.png")
        {
            string filename = RandomString(20);
            if (openFileDialog.FileName != null && openFileDialog.FileName != "")
            {
                string path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName +
                    "\\Asset\\Products\\" + filename;
                File.Copy(openFileDialog.FileName, path, true);
            }
            else
            {
                filename = defaultFilename;
            }

            return filename;
        }

        public bool Validation()
        {
            string messages = "";
            if (productFormPage.ProductName.Text.ToString() == "")
            {
                messages = "Name is required";
            }
            else if (productFormPage.ProductPrice.Text.ToString() == "")
            {
                messages = "Price is required";
            }
            else if (productFormPage.FoodRadioButton.IsChecked == false && productFormPage.BeverageRadioButton.IsChecked == false)
            {
                messages = "Category is required";
            }

            if (messages != "")
            {
                MessageBox.Show(messages, "Validation Error");
                return false;
            }

            return true;
        }

        public void CreateProduct()
        {
            parentWindow = (MainWindow)Window.GetWindow(productListPage);

            productFormPage = new View.Product.FormPage();
            parentWindow.MainWindowFrame.Navigate(productFormPage);
        }

        public bool StoreProduct()
        {
            if (Validation() == false)
            {
                return false;
            }

            string filename = CopyFileDialog();
            productModel.Name = productFormPage.ProductName.Text;
            productModel.Description = productFormPage.ProductDescription.Text;
            productModel.Filename = filename;
            productModel.Price = float.Parse(productFormPage.ProductPrice.Text.ToString());
            if (productFormPage.FoodRadioButton.IsChecked == true)
            {
                productModel.Category = "F";
            }
            else
            {
                productModel.Category = "B";
            }

            ProductModel productData = productModel.Insert();

            if (productData == null)
            {
                MessageBox.Show("Add Product Failed", "Error");
                return false;
            }

            parentWindow = (MainWindow)Window.GetWindow(productFormPage);
            parentWindow.MainWindowFrame.Navigate(new View.Product.ListPage());
            return true;
        }

        public void EditProduct(int ProductId)
        {
            productModel.Id = ProductId;
            ProductModel productData = productModel.GetById();

            if (productData == null)
            {
                MessageBox.Show("Product Not Found", "Error");
                return;
            }

            parentWindow = (MainWindow)Window.GetWindow(productListPage);

            productFormPage = new View.Product.FormPage();
            productFormPage.DataContext = productData;
            productFormPage.FoodRadioButton.IsChecked = productData.Category == "F";
            productFormPage.BeverageRadioButton.IsChecked = productData.Category == "B";
            parentWindow.MainWindowFrame.Navigate(productFormPage);
        }

        public bool UpdateProduct(int ProductId)
        {
            productModel.Id = ProductId;
            ProductModel productData = productModel.GetById();

            if (productData == null)
            {
                MessageBox.Show("Product Not Found", "Error");
                return false;
            }

            if (Validation() == false)
            {
                return false;
            }

            string filename = CopyFileDialog(productData.Filename);
            productData.Name = productFormPage.ProductName.Text;
            productData.Description = productFormPage.ProductDescription.Text;
            productData.Filename = filename;
            productData.Price = float.Parse(productFormPage.ProductPrice.Text.ToString());
            if (productFormPage.FoodRadioButton.IsChecked == true)
            {
                productData.Category = "F";
            }
            else
            {
                productData.Category = "B";
            }

            productData = productData.Update();

            if (productData == null)
            {
                MessageBox.Show("Update Product Failed", "Error");
                return false;
            }


            parentWindow = (MainWindow)Window.GetWindow(productFormPage);

            productListPage = new View.Product.ListPage();
            parentWindow.MainWindowFrame.Navigate(productListPage);
            return true;
        }

        public bool DeleteProduct(int ProductId)
        {
            productModel.Id = ProductId;
            ProductModel productData = productModel.GetById();

            if (productData == null)
            {
                MessageBox.Show("Product Not Found", "Error");
                return false;
            }

            MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);

            if (messageBoxResult == MessageBoxResult.Yes)
            {
                bool result = productData.Delete();

                if (result)
                {
                    ShowProductListView(productData.Category);
                }

                return result;
            }

            return false;
        }
    }
}
