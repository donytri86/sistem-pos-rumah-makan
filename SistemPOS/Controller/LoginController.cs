﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using SistemPOS.Model;
using SistemPOS.View;

namespace SistemPOS.Controller
{
    class LoginController
    {
        LoginWindow loginWindow;
        UserModel userModel;

        public LoginController(LoginWindow view)
        {
            this.loginWindow = view;
        }

        public void LoginAction()
        {
            userModel = new UserModel();
            userModel.UserName = loginWindow.LoginUsername.Text.ToString();
            userModel.Password = loginWindow.LoginPassword.Password.ToString();

            userModel = userModel.GetLogin();

            if (userModel == null)
            {
                MessageBox.Show("Username/Password Incorrect!", "Error");
                loginWindow.LoginUsername.Focus();
                return;
            }
            Application.Current.Properties["LoggedInFullname"] = userModel.FullName;

            MainWindow mainWindow = new MainWindow();
            mainWindow.LoggedInFullname.Content = Application.Current.Properties["LoggedInFullname"];
            mainWindow.Show();
            loginWindow.Close();
        }
    }
}
