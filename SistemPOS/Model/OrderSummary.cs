﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemPOS.Model
{
    class OrderSummary
    {
        public int OrderSummaryCount { get; set; }
        public float OrderSummaryGrandTotal { get; set; }
    }
}
