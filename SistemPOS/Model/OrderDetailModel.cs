﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows;

namespace SistemPOS.Model
{
    class OrderDetailModel
    {
        MainModel mainModal;
        ProductModel productModel;

        public string table = "order_details";

        public int Id { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public float Price { get; set; }
        public int Qty { get; set; }
        public float TotalPrice { get; set; }
        public ProductModel Product { get; set; }

        public OrderDetailModel()
        {
            mainModal = new MainModel();
            productModel = new ProductModel();
        }

        public List<OrderDetailModel> GetByOrderId()
        {
            string query = "SELECT od.id, od.product_id, p.name, p.description, p.category, p.filename, od.price, od.qty, od.total_price " +
                "FROM " + table + " od " +
                "JOIN " + productModel.table + " p ON od.product_id = p.id " +
                "WHERE order_id = " + OrderId;
            DataSet dataset = mainModal.Query(table, query);

            List<OrderDetailModel> OrderDetailList = null;
            if (dataset != null)
            {
                OrderDetailList = dataset.Tables[0].AsEnumerable()
                    .Select(dataRow => new OrderDetailModel
                    {
                        Id = dataRow.Field<int>("id"),
                        ProductId = dataRow.Field<int>("product_id"),
                        Qty = dataRow.Field<int>("qty"),
                        Price = dataRow.Field<float>("price"),
                        TotalPrice = dataRow.Field<float>("total_price"),
                        Product = new ProductModel
                        {
                            Id = dataRow.Field<int>("product_id"),
                            Name = dataRow.Field<string>("name"),
                            Description = dataRow.Field<string>("description"),
                            Category = dataRow.Field<string>("category"),
                            Filename = dataRow.Field<string>("filename"),
                            Price = dataRow.Field<float>("price"),
                        }
                    }).ToList();
            }

            return OrderDetailList;
        }

        public OrderDetailModel Insert()
        {
            string data = "'" + OrderId + "', '" + ProductId + "', '" + Price + "', '" + Qty + "', '" + TotalPrice + "'";

            DataSet dataset = mainModal.Insert(table, data);

            OrderDetailModel orderDetailModel = null;
            if (dataset != null && dataset.Tables[0].Rows.Count == 1)
            {
                orderDetailModel = new OrderDetailModel
                {
                    Id = (int)dataset.Tables[0].Rows[0][0],
                    OrderId = (int)dataset.Tables[0].Rows[0][1],
                    ProductId = (int)dataset.Tables[0].Rows[0][2],
                    Price = (float)dataset.Tables[0].Rows[0][3],
                    Qty = (int)dataset.Tables[0].Rows[0][4],
                    TotalPrice = (float)dataset.Tables[0].Rows[0][5],
                };
            }

            return orderDetailModel;
        }
    }
}
