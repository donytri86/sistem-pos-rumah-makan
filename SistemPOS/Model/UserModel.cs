﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows;

namespace SistemPOS.Model
{
    class UserModel
    {
        MainModel mainModal;

        public string table = "users";

        public int Id { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        
        public UserModel()
        {
            mainModal = new MainModel();
        }

        public List<UserModel> GetAll(string Search = "")
        {
            string where = " (fullname like '%" + Search + "%' or username like '%" + Search + "%') ";

            DataSet dataset = mainModal.Select(table, where);

            List<UserModel> UserList = null;
            if (dataset != null)
            {
                UserList = dataset.Tables[0].AsEnumerable()
                    .Select(dataRow => new UserModel
                    {
                        Id = dataRow.Field<int>("id"),
                        FullName = dataRow.Field<string>("fullname"),
                        UserName = dataRow.Field<string>("username"),
                        Password = dataRow.Field<string>("password")
                    }).ToList();
            }

            return UserList;
        }

        public UserModel GetLogin()
        {
            string where = "username = '" + UserName + "' AND password = '" + Password + "'";

            DataSet dataset = mainModal.Select(table, where);

            UserModel userData = null;
            if (dataset != null && dataset.Tables[0].Rows.Count == 1)
            {
                userData = new UserModel
                {
                    Id = (int)dataset.Tables[0].Rows[0][0],
                    FullName = dataset.Tables[0].Rows[0][1].ToString(),
                    UserName = dataset.Tables[0].Rows[0][2].ToString(),
                };
            }

            return userData;
        }

        public UserModel GetById()
        {
            string where = "id = '" + Id + "'";

            DataSet dataset = mainModal.Select(table, where);

            UserModel userData = null;
            if (dataset != null && dataset.Tables[0].Rows.Count == 1)
            {
                userData = new UserModel
                {
                    Id = (int)dataset.Tables[0].Rows[0][0],
                    FullName = dataset.Tables[0].Rows[0][1].ToString(),
                    UserName = dataset.Tables[0].Rows[0][2].ToString(),
                    Password = dataset.Tables[0].Rows[0][3].ToString(),
                };
            }

            return userData;
        }

        public UserModel Insert()
        {
            string data = "'" + FullName + "', '" + UserName + "', '" + Password + "'";

            DataSet dataset = mainModal.Insert(table, data);

            UserModel userData = null;
            if (dataset != null && dataset.Tables[0].Rows.Count == 1)
            {
                userData = new UserModel
                {
                    Id = (int)dataset.Tables[0].Rows[0][0],
                    FullName = dataset.Tables[0].Rows[0][1].ToString(),
                    UserName = dataset.Tables[0].Rows[0][2].ToString(),
                    Password = dataset.Tables[0].Rows[0][3].ToString(),
                };
            }

            return userData;
        }

        public UserModel Update()
        {
            string data = "fullname = '" + FullName + "', username = '" + UserName + "', password = '" + Password + "'";
            string where = "id = '" + Id + "'";

            DataSet dataset = mainModal.Update(table, data, where);

            UserModel userData = null;
            if (dataset != null && dataset.Tables[0].Rows.Count == 1)
            {
                userData = new UserModel
                {
                    Id = (int)dataset.Tables[0].Rows[0][0],
                    FullName = dataset.Tables[0].Rows[0][1].ToString(),
                    UserName = dataset.Tables[0].Rows[0][2].ToString(),
                    Password = dataset.Tables[0].Rows[0][3].ToString(),
                };
            }

            return userData;
        }

        public bool Delete()
        {
            string where = "id = '" + Id + "'";

            return mainModal.Delete(table, where);
        }
    }
}
