﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows;
using System.Windows.Media.Imaging;
using System.IO;

namespace SistemPOS.Model
{
    class ProductModel
    {
        MainModel mainModal;

        public string table = "products";

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }

        private string _Filename;
        public string Filename {
            get
            {
                return _Filename;
            }
            set
            {
                _Filename = value;

                string path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName +
                "\\Asset\\Products\\" + _Filename;
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(path);
                bitmap.EndInit();
                ProductFile = bitmap;
            }
        }

        public BitmapImage ProductFile { get; set; }
        public float Price { get; set; }

        public ProductModel()
        {
            mainModal = new MainModel();
        }

        public List<ProductModel> GetByCategory(string Search = "")
        {
            string where = "category = '" + Category + "' and (name like '%" + Search + "%' or description like '%" + Search + "%') ";

            DataSet dataset = mainModal.Select(table, where);

            List<ProductModel> ProductList = dataset.Tables[0].AsEnumerable()
                .Select(dataRow => new ProductModel
                {
                    Id = dataRow.Field<int>("id"),
                    Name = dataRow.Field<string>("name"),
                    Description = dataRow.Field<string>("description"),
                    Category = dataRow.Field<string>("category"),
                    Filename = dataRow.Field<string>("filename"),
                    Price = dataRow.Field<float>("price"),
                }).ToList();

            return ProductList;
        }

        public ProductModel GetById()
        {
            string where = "id = '" + Id + "'";

            DataSet dataset = mainModal.Select(table, where);

            ProductModel productData = null;
            if (dataset != null && dataset.Tables[0].Rows.Count == 1)
            {
                productData = new ProductModel
                {
                    Id = (int)dataset.Tables[0].Rows[0][0],
                    Name = dataset.Tables[0].Rows[0][1].ToString(),
                    Description = dataset.Tables[0].Rows[0][2].ToString(),
                    Category = dataset.Tables[0].Rows[0][3].ToString(),
                    Filename = dataset.Tables[0].Rows[0][4].ToString(),
                    Price = (float)dataset.Tables[0].Rows[0][5],
                };
            }

            return productData;
        }

        public ProductModel Insert()
        {
            string data = "'" + Name + "', '" + Description + "', '" + Category + "', '" + Filename + "', '" + Price + "'";

            DataSet dataset = mainModal.Insert(table, data);

            ProductModel productData = null;
            if (dataset != null && dataset.Tables[0].Rows.Count == 1)
            {
                productData = new ProductModel
                {
                    Id = (int)dataset.Tables[0].Rows[0][0],
                    Name = dataset.Tables[0].Rows[0][1].ToString(),
                    Description = dataset.Tables[0].Rows[0][2].ToString(),
                    Category = dataset.Tables[0].Rows[0][3].ToString(),
                    Filename = dataset.Tables[0].Rows[0][4].ToString(),
                    Price = (float)dataset.Tables[0].Rows[0][5],
                };
            }

            return productData;
        }

        public ProductModel Update()
        {
            string data = "name = '" + Name + "', description = '" + Description + "', category = '" + Category + "', filename = '" + Filename + "', price = '" + Price + "'";
            string where = "id = '" + Id + "'";

            DataSet dataset = mainModal.Update(table, data, where);

            ProductModel productData = null;
            if (dataset != null && dataset.Tables[0].Rows.Count == 1)
            {
                productData = new ProductModel
                {
                    Id = (int)dataset.Tables[0].Rows[0][0],
                    Name = dataset.Tables[0].Rows[0][1].ToString(),
                    Description = dataset.Tables[0].Rows[0][2].ToString(),
                    Category = dataset.Tables[0].Rows[0][3].ToString(),
                    Filename = dataset.Tables[0].Rows[0][4].ToString(),
                    Price = (float)dataset.Tables[0].Rows[0][5],
                };
            }

            return productData;
        }

        public bool Delete()
        {
            string where = "id = '" + Id + "'";

            return mainModal.Delete(table, where);
        }
    }
}
