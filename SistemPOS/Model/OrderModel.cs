﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows;

namespace SistemPOS.Model
{
    class OrderModel
    {
        MainModel mainModal;

        CartModel cartModel;
        OrderDetailModel orderDetailModel;

        public string table = "orders";

        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string TransactionTime { get; set; }
        public float GrandTotal { get; set; }
        public float Payment { get; set; }
        public float Change { get; set; }

        public List<OrderDetailModel> OrderDetails { get; set; }

        public OrderModel()
        {
            mainModal = new MainModel();

            cartModel = new CartModel();
            orderDetailModel = new OrderDetailModel();

            OrderDetails = new List<OrderDetailModel>();
        }

        public List<OrderModel> GetAll(string StartDate = null, string EndDate = null)
        {
            string where = null;

            if (StartDate != null && EndDate != null && StartDate != "" && EndDate != "")
            {
                StartDate += " 00:00:00";
                EndDate += " 23:59:59";
                where = " transaction_time BETWEEN '" + StartDate + "' AND '" + EndDate + "' ";
            }

            DataSet dataset = mainModal.Select(table, where);

            List<OrderModel> orderList = null;
            if (dataset != null)
            {
                orderList = dataset.Tables[0].AsEnumerable()
                   .Select(dataRow => new OrderModel
                   {
                       Id = (int)dataRow[0],
                       CustomerName = dataRow[1].ToString(),
                       TransactionTime = dataRow[2].ToString(),
                       GrandTotal = (float)dataRow[3],
                       Payment = (float)dataRow[4],
                       Change = (float)dataRow[5],
                   }).ToList();
            }

            return orderList;
        }

        public OrderSummary GetSummary(string StartDate = null, string EndDate = null)
        {
            string where = "";

            if (StartDate != null && EndDate != null && StartDate != "" && EndDate != "")
            {
                StartDate += " 00:00:00";
                EndDate += " 23:59:59";
                where = "WHERE transaction_time BETWEEN '" + StartDate + "' AND '" + EndDate + "' ";
            }

            string query = "SELECT COUNT(*) as order_count, ISNULL(SUM(grand_total), 0) as grand_total_sum FROM " + table + " " + where + ";";

            DataSet dataset = mainModal.Query(table, query);

            OrderSummary orderSummary = null;
            if (dataset != null && dataset.Tables[0].Rows.Count == 1)
            {
                orderSummary = new OrderSummary
                {
                    OrderSummaryCount = int.Parse(dataset.Tables[0].Rows[0][0].ToString()),
                    OrderSummaryGrandTotal = float.Parse(dataset.Tables[0].Rows[0][1].ToString()),
                };
            }

            return orderSummary;
        }

        public OrderModel GetById()
        {
            string where = "id = '" + Id + "'";

            DataSet dataset = mainModal.Select(table, where);

            OrderModel orderData = null;
            if (dataset != null && dataset.Tables[0].Rows.Count == 1)
            {
                orderData = new OrderModel
                {
                    Id = (int)dataset.Tables[0].Rows[0][0],
                    CustomerName = dataset.Tables[0].Rows[0][1].ToString(),
                    TransactionTime = dataset.Tables[0].Rows[0][2].ToString(),
                    GrandTotal = (float)dataset.Tables[0].Rows[0][3],
                    Payment = (float)dataset.Tables[0].Rows[0][4],
                    Change = (float)dataset.Tables[0].Rows[0][5],
                };

                orderDetailModel.OrderId = orderData.Id;
                orderData.OrderDetails = orderDetailModel.GetByOrderId();
            }

            return orderData;
        }

        public OrderModel Insert()
        {
            List<CartModel> cartData = cartModel.GetAll();

            OrderModel orderData = null;
            if (cartData.Count > 0)
            {
                string data = "'" + CustomerName + "', '" + GrandTotal + "', '" + Payment + "', '" + Change + "'";
                string columns = "customer_name, grand_total, payment, change";

                DataSet dataset = mainModal.Insert(table, data, columns);

                if (dataset != null && dataset.Tables[0].Rows.Count == 1)
                {
                    orderData = new OrderModel
                    {
                        Id = (int)dataset.Tables[0].Rows[0][0],
                        CustomerName = dataset.Tables[0].Rows[0][1].ToString(),
                        TransactionTime = dataset.Tables[0].Rows[0][2].ToString(),
                        GrandTotal = (float)dataset.Tables[0].Rows[0][3],
                        Payment = (float)dataset.Tables[0].Rows[0][4],
                        Change = (float)dataset.Tables[0].Rows[0][5],
                    };

                    foreach (CartModel row in cartData)
                    {
                        orderDetailModel.OrderId = orderData.Id;
                        orderDetailModel.ProductId = row.ProductId;
                        orderDetailModel.Price = row.Product.Price;
                        orderDetailModel.Qty = row.Qty;
                        orderDetailModel.TotalPrice = row.Product.Price * row.Qty;
                        
                        OrderDetailModel orderDetailData = orderDetailModel.Insert();

                        if (orderDetailData != null)
                        {
                            orderData.OrderDetails.Add(orderDetailData);
                        }
                    }
                }
            }

            if (orderData != null)
            {
                cartModel.Clear();
            }

            return orderData;
        }
    }
}
