﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace SistemPOS.Model
{
    class MainModel
    {
        private static SqlConnection connection;

        private SqlCommand command;

        private bool result;

        public static SqlConnection GetConnection()
        {
            connection = new SqlConnection();

            connection.ConnectionString = "Data Source = DESKTOP-KII3K9I;" +
                                          "Initial Catalog = pos_ccb;" +
                                          "Integrated Security = True";

            return connection;
        }

        public MainModel()
        {
            GetConnection();
        }

        public DataSet Select(string table, string where = null)
        {
            DataSet dataset = new DataSet();

            try
            {
                connection.Open();
                command = new SqlCommand();
                command.Connection = connection;
                command.CommandType = CommandType.Text;
                if (where == null)
                {
                    command.CommandText = "SELECT * FROM " + table;
                }
                else
                {
                    command.CommandText = "SELECT * FROM " + table + " WHERE " + where;
                }

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataset, table);
            }
            catch (SqlException)
            {
                dataset = null;
            }

            connection.Close();
            return dataset;
        }

        public DataSet Query(string table, string query)
        {
            DataSet dataset = new DataSet();

            try
            {
                connection.Open();
                command = new SqlCommand();
                command.Connection = connection;
                command.CommandType = CommandType.Text;
                command.CommandText = query;
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataset, table);
            }
            catch (SqlException)
            {
                dataset = null;
            }

            connection.Close();
            return dataset;
        }

        public DataSet Insert(string table, string data, string columns = null)
        {
            DataSet dataset = new DataSet();

            try
            {
                connection.Open();
                command = new SqlCommand();
                command.Connection = connection;
                command.CommandType = CommandType.Text;

                if (columns == null)
                {
                    command.CommandText = "INSERT INTO " + table + " VALUES (" + data + "); SELECT * FROM " + table + " WHERE id = SCOPE_IDENTITY();";
                } else
                {
                    command.CommandText = "INSERT INTO " + table + " (" + columns + ") VALUES (" + data + "); SELECT * FROM " + table + " WHERE id = SCOPE_IDENTITY();";
                }
                
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataset, table);
            }
            catch (SqlException)
            {
                dataset = null;
            }

            connection.Close();
            return dataset;
        }

        public DataSet Update(string table, string data, string where)
        {
            DataSet dataset = new DataSet();

            try
            {
                connection.Open();
                command = new SqlCommand();
                command.Connection = connection;
                command.CommandType = CommandType.Text;

                command.CommandText = "UPDATE " + table + " SET " + data + " WHERE " + where + "; SELECT * FROM " + table + " WHERE " + where;

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataset, table);
            }
            catch (SqlException)
            {
                dataset = null;
            }

            connection.Close();
            return dataset;
        }

        public bool Delete(string table, string where)
        {
            result = false;

            try
            {
                string query = "DELETE FROM " + table + " WHERE " + where;
                connection.Open();
                command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = query;
                command.ExecuteNonQuery();
                result = true;
            }
            catch (SqlException)
            {
                result = false;
            }

            connection.Close();
            return result;
        }
    }
}
