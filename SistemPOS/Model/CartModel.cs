﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows;

namespace SistemPOS.Model
{
    class CartModel
    {
        MainModel mainModal;
        ProductModel productModel;

        public string table = "carts";

        public int Id { get; set; }
        public int ProductId { get; set; }
        public int Qty { get; set; }
        public float Price { get; set; }
        public float TotalPrice { get; set; }
        public ProductModel Product { get; set; }

        public CartModel()
        {
            mainModal = new MainModel();
            productModel = new ProductModel();
        }

        public List<CartModel> GetAll()
        {
            string query = "SELECT c.id, c.product_id, p.name, p.description, p.filename, p.category, p.price, c.qty " +
                "FROM " + table + " c " +
                "JOIN " + productModel.table + " p ON c.product_id = p.id ";
            DataSet dataset = mainModal.Query(table, query);

            List<CartModel> CartList = null;
            
            if (dataset != null)
            {
                CartList = dataset.Tables[0].AsEnumerable()
                    .Select(dataRow => new CartModel
                    {
                        Id = dataRow.Field<int>("id"),
                        ProductId = dataRow.Field<int>("product_id"),
                        Qty = dataRow.Field<int>("qty"),
                        Price = dataRow.Field<float>("price"),
                        TotalPrice = dataRow.Field<float>("price") * dataRow.Field<int>("qty"),
                        Product = new ProductModel
                        {
                            Id = dataRow.Field<int>("product_id"),
                            Name = dataRow.Field<string>("name"),
                            Description = dataRow.Field<string>("description"),
                            Category = dataRow.Field<string>("category"),
                            Filename = dataRow.Field<string>("filename"),
                            Price = dataRow.Field<float>("price"),
                        }
                    }).ToList();
            }

            return CartList;
        }

        public CartModel GetByProductId()
        {
            string where = "product_id = '" + ProductId + "'";

            DataSet dataset = mainModal.Select(table, where);

            CartModel cartData = null;
            if (dataset != null && dataset.Tables[0].Rows.Count == 1)
            {
                cartData = new CartModel()
                {
                    Id = (int)dataset.Tables[0].Rows[0][0],
                    ProductId = (int)dataset.Tables[0].Rows[0][1],
                    Qty = (int)dataset.Tables[0].Rows[0][2],
                };
            }

            return cartData;
        }

        public CartModel Insert()
        {
            string data = "'" + ProductId + "', '" + Qty + "'";

            DataSet dataset = mainModal.Insert(table, data);

            CartModel cartData = null;
            if (dataset != null && dataset.Tables[0].Rows.Count == 1)
            {
                cartData = new CartModel()
                {
                    Id = (int)dataset.Tables[0].Rows[0][0],
                    ProductId = (int)dataset.Tables[0].Rows[0][1],
                    Qty = (int)dataset.Tables[0].Rows[0][2],
                };
            }

            return cartData;
        }

        public CartModel Update()
        {
            string data = "qty = '" + Qty + "'";
            string where = "product_id = '" + ProductId + "'";

            DataSet dataset = mainModal.Update(table, data, where);

            CartModel cartData = null;
            if (dataset != null && dataset.Tables[0].Rows.Count == 1)
            {
                cartData = new CartModel()
                {
                    Id = (int)dataset.Tables[0].Rows[0][0],
                    ProductId = (int)dataset.Tables[0].Rows[0][1],
                    Qty = (int)dataset.Tables[0].Rows[0][2],
                };
            }

            return cartData;
        }

        public bool Delete()
        {
            string where = "product_id = '" + ProductId + "'";

            return mainModal.Delete(table, where);
        }

        public bool Clear()
        {
            string where = "1 = 1";

            return mainModal.Delete(table, where);
        }
    }
}
