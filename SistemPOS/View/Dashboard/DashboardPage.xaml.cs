﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SistemPOS.View.Dashboard
{
    /// <summary>
    /// Interaction logic for DashboardPage.xaml
    /// </summary>
    public partial class DashboardPage : Page
    {
        MainWindow parentWindow;

        public DashboardPage()
        {
            InitializeComponent();
        }

        private void DashboardMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Button _button = (Button)sender;
            string MenuAction = _button.Name;
            
            parentWindow = (MainWindow)Window.GetWindow(this);

            bool menuExists = true;
            switch (MenuAction)
            {
                case "menuPOS":
                    POSWindow posWindow = new POSWindow();
                    posWindow.Show();
                    parentWindow.Close();

                    break;

                case "menuDashboard":
                    parentWindow.MainWindowFrame.Navigate(new Dashboard.DashboardPage());

                    break;

                case "menuProduct":
                    parentWindow.MainWindowFrame.Navigate(new Product.ListPage());

                    break;

                case "menuUser":
                    parentWindow.MainWindowFrame.Navigate(new User.ListPage());

                    break;

                case "menuReport":
                    parentWindow.MainWindowFrame.Navigate(new Report.ListPage());

                    break;

                default:
                    menuExists = false;
                    break;
            }

            if (menuExists)
            {
                SetActiveMenuItem(MenuAction);
            }
        }

        private void SetActiveMenuItem(string menuName = "menuDashboard")
        {
            BrushConverter _brushConverter = new BrushConverter();

            parentWindow.menuDashboard.SetResourceReference(Control.StyleProperty, "SidebarMenuItem");
            parentWindow.menuProduct.SetResourceReference(Control.StyleProperty, "SidebarMenuItem");
            parentWindow.menuUser.SetResourceReference(Control.StyleProperty, "SidebarMenuItem");
            parentWindow.menuReport.SetResourceReference(Control.StyleProperty, "SidebarMenuItem");

            Button activeMenu = (Button)parentWindow.FindName(menuName);
            if (activeMenu != null)
            {
                //activeMenu.Background = (Brush)_brushConverter.ConvertFrom("#ADE2FF");

                activeMenu.SetResourceReference(Control.StyleProperty, "SidebarMenuItemActive");
            }
        }
    }
}
