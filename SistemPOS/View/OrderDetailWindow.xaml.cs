﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SistemPOS.Controller;

namespace SistemPOS.View
{
    /// <summary>
    /// Interaction logic for OrderDetailWindow.xaml
    /// </summary>
    public partial class OrderDetailWindow : Window
    {
        ReportController reportController;
        public int orderId { get; set; }
        public OrderDetailWindow()
        {
            InitializeComponent();

            reportController = new ReportController(this);
        }

        public void ShowOrderDetail()
        {
            reportController.ReportDetail(orderId, this);
        }
    }
}
