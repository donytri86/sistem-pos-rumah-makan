﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SistemPOS.Controller;

namespace SistemPOS.View.Product
{
    /// <summary>
    /// Interaction logic for FormPage.xaml
    /// </summary>
    public partial class FormPage : Page
    {
        ProductController productController;

        public FormPage()
        {
            InitializeComponent();

            productController = new ProductController(this);
        }

        private void TextBoxNumberOnly(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void SaveProductButton_Click(object sender, RoutedEventArgs e)
        {
            Button _button = (Button)sender;
            
            if (_button.Tag == null)
            {
                productController.StoreProduct();
            }
            else
            {
                int ProductId = (int)_button.Tag;
                productController.UpdateProduct(ProductId);
            }
        }

        private void BrowseImageButton_Click(object sender, RoutedEventArgs e)
        {
            productController.openFileDialog.Title = "Select Image";
            productController.openFileDialog.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";

            if (productController.openFileDialog.ShowDialog() == true)
            {
                string selectedName = productController.openFileDialog.FileName;

                BitmapImage bitmap = new BitmapImage();

                bitmap.BeginInit();
                bitmap.UriSource = new Uri(selectedName);
                bitmap.EndInit();

                ProductImage.ImageSource = bitmap;
            }
            else
            {
                ProductImage.ImageSource = null;
            }
        }
    }
}
