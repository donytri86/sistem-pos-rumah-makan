﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SistemPOS.Controller;

namespace SistemPOS.View.Product
{
    /// <summary>
    /// Interaction logic for FoodPage.xaml
    /// </summary>
    public partial class ListPage : Page
    {
        ProductController productController;

        string CurrentCategory = "F";

        public ListPage()
        {
            InitializeComponent();

            productController = new ProductController(this);
            productController.ShowProductListView("F", "");
        }

        private void ProductCategoryButton_Click(object sender, RoutedEventArgs e)
        {
            Button _button = (Button)sender;
            string Category = _button.Tag.ToString();

            CurrentCategory = Category;
            ProductSearch.Text = "";
            productController.ShowProductListView(CurrentCategory);
        }

        private void ProductEditButton_Click(object sender, RoutedEventArgs e)
        {
            Button _button = (Button)sender;
            int ProductId = (int)_button.Tag;

            productController.EditProduct(ProductId);
        }

        private void ProductDeleteButton_Click(object sender, RoutedEventArgs e)
        {
            Button _button = (Button)sender;
            int ProductId = (int)_button.Tag;

            productController.DeleteProduct(ProductId);
        }

        private void ProductCreateButton_Click(object sender, RoutedEventArgs e)
        {
            productController.CreateProduct();
        }

        private void ProductSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            string Search = ProductSearch.Text.ToString();

            productController.ShowProductListView(CurrentCategory, Search);
        }
    }
}
