﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SistemPOS.Controller;

namespace SistemPOS.View.User
{
    /// <summary>
    /// Interaction logic for ListPage.xaml
    /// </summary>
    public partial class ListPage : Page
    {
        UserController userController;

        public ListPage()
        {
            InitializeComponent();

            userController = new UserController(this);
            userController.ShowUserListView();
        }

        private void UserCreateButton_Click(object sender, RoutedEventArgs e)
        {
            userController.CreateUser();
        }

        private void UserEditButton_Click(object sender, RoutedEventArgs e)
        {
            Button _button = (Button)sender;
            int UserId = (int)_button.Tag;

            userController.EditUser(UserId);
        }

        private void UserDeleteButton_Click(object sender, RoutedEventArgs e)
        {
            Button _button = (Button)sender;
            int UserId = (int)_button.Tag;

            userController.DeleteUser(UserId);
        }

        private void ProductSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            string Search = UserSearch.Text.ToString();

            userController.ShowUserListView(Search);
        }
    }
}
