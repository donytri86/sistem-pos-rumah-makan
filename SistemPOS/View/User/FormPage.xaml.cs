﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SistemPOS.Controller;

namespace SistemPOS.View.User
{
    /// <summary>
    /// Interaction logic for FormPage.xaml
    /// </summary>
    public partial class FormPage : Page
    {
        UserController userController;

        public FormPage()
        {
            InitializeComponent();

            userController = new UserController(this);
        }

        private void SaveUserButton_Click(object sender, RoutedEventArgs e)
        {
            Button _button = (Button)sender;

            if (_button.Tag == null)
            {
                userController.StoreUser();
            }
            else
            {
                int UserId = (int)_button.Tag;
                userController.UpdateUser(UserId);
            }
        }
    }
}
