﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SistemPOS.Controller;

namespace SistemPOS.View.Report
{
    /// <summary>
    /// Interaction logic for ListPage.xaml
    /// </summary>
    public partial class ListPage : Page
    {
        ReportController reportController;

        public ListPage()
        {
            InitializeComponent();

            reportController = new ReportController(this);
            reportController.ShowReportListView();
            reportController.ShowReportSummary();
        }

        private void ReportDetailButton_Click(object sender, RoutedEventArgs e)
        {
            Button _button = (Button)sender;

            if (_button.Tag != null)
            {
                int ProductId = (int)_button.Tag;

                OrderDetailWindow orderDetailWindow = new OrderDetailWindow();
                orderDetailWindow.orderId = ProductId;
                orderDetailWindow.ShowOrderDetail();
            }

        }

        private void DatePicker_CalendarClosed(object sender, RoutedEventArgs e)
        {
            if(ReportStartDate.SelectedDate <= ReportEndDate.SelectedDate)
            {
                string StartDate = String.Format("{0:yyyy-MM-dd}", ReportStartDate.SelectedDate);
                string EndDate = String.Format("{0:yyyy-MM-dd}", ReportEndDate.SelectedDate);

                reportController.ShowReportListView(StartDate, EndDate);
                reportController.ShowReportSummary(StartDate, EndDate);
            }
        }
    }

    
}
