﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SistemPOS.Controller;

namespace SistemPOS.View
{
    /// <summary>
    /// Interaction logic for POSWindow.xaml
    /// </summary>
    public partial class POSWindow : Window
    {
        const int WM_SYSCOMMAND = 0x0112;
        const int SC_MOVE = 0xF010;

        POSController posController;

        string CurrentCategory = "F";
        public POSWindow()
        {
            InitializeComponent();

            SourceInitialized += POSWindow_SourceInitialized;

            posController = new POSController(this);

            posController.ShowProductListView("F");
            posController.ShowCartListView();
        }

        private void POSWindow_SourceInitialized(object sender, EventArgs e)
        {
            WindowInteropHelper helper = new WindowInteropHelper(this);
            HwndSource source = HwndSource.FromHwnd(helper.Handle);
            source.AddHook(WindowProtect);
        }

        private IntPtr WindowProtect(IntPtr hwnd, int message, IntPtr wParam, IntPtr lParam, ref bool handled)
        {

            switch (message)
            {
                case WM_SYSCOMMAND:
                    int command = wParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                    {
                        handled = true;
                    }
                    break;
                default:
                    break;
            }
            return IntPtr.Zero;
        }

        private void ProductCategoryButton_Click(object sender, RoutedEventArgs e)
        {
            Button _button = (Button)sender;
            string Category = _button.Tag.ToString();

            CurrentCategory = Category;
            ProductSearch.Text = "";
            posController.ShowProductListView(Category);
        }

        private void ProductItemButton_Click(object sender, RoutedEventArgs e)
        {
            Button _button = (Button)sender;
            int ProductId = (int)_button.Tag;

            posController.AddToCart(ProductId);
        }

        private void CartItemDecreaseButton_Click(object sender, RoutedEventArgs e)
        {
            Button _button = (Button)sender;
            int ProductId = (int)_button.Tag;

            posController.AddToCart(ProductId, -1);
        }

        private void CartItemIncreaseButton_Click(object sender, RoutedEventArgs e)
        {
            Button _button = (Button)sender;
            int ProductId = (int)_button.Tag;

            posController.AddToCart(ProductId, 1);
        }

        private void OrderCustomerName_TextChanged(object sender, TextChangedEventArgs e)
        {
            string customerName = OrderCustomerName.Text.ToString();

            posController.UpdateCustomerName(customerName);
        }

        private void OrderPayment_TextChanged(object sender, TextChangedEventArgs e)
        {
            float payment = 0;
            if (OrderPayment.Text.ToString() != "")
            {
                payment = float.Parse(OrderPayment.Text.ToString());
            }

            posController.UpdateCartSummary(payment);
        }

        private void TextBoxNumberOnly(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void OrderButton_Click(object sender, RoutedEventArgs e)
        {
            posController.CreateOrder();
        }

        private void ExitPOSButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void ProductSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            string Search = ProductSearch.Text.ToString();

            posController.ShowProductListView(CurrentCategory, Search);
        }
    }
}
