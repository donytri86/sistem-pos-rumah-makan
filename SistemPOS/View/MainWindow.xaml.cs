﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SistemPOS.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            LoggedInFullname.Content = Application.Current.Properties["LoggedInFullname"];
        }

        private void SidebarMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Button _button = (Button)sender;
            string MenuAction = _button.Name;

            bool menuExists = true;
            switch (MenuAction)
            {
                case "menuPOS":
                    POSWindow posWindow = new POSWindow();
                    posWindow.Show();
                    this.Close();

                    break;

                case "menuDashboard":
                    MainWindowFrame.Navigate(new Dashboard.DashboardPage());

                    break;

                case "menuProduct":
                    MainWindowFrame.Navigate(new Product.ListPage());

                    break;

                case "menuUser":
                    MainWindowFrame.Navigate(new User.ListPage());

                    break;

                case "menuReport":
                    MainWindowFrame.Navigate(new Report.ListPage());

                    break;

                case "menuLogout":
                    LoginWindow loginWindow = new LoginWindow();
                    loginWindow.Show();
                    this.Close();

                    break;

                default:
                    menuExists = false;
                    break;
            }

            if (menuExists)
            {
                SetActiveMenuItem(MenuAction);
            }
        }

        private void SetActiveMenuItem(string menuName = "menuDashboard")
        {
            BrushConverter _brushConverter = new BrushConverter();

            menuDashboard.SetResourceReference(Control.StyleProperty, "SidebarMenuItem");
            menuProduct.SetResourceReference(Control.StyleProperty, "SidebarMenuItem");
            menuUser.SetResourceReference(Control.StyleProperty, "SidebarMenuItem");
            menuReport.SetResourceReference(Control.StyleProperty, "SidebarMenuItem");

            Button activeMenu = (Button)this.FindName(menuName);
            if (activeMenu != null)
            {
                activeMenu.SetResourceReference(Control.StyleProperty, "SidebarMenuItemActive");
            }
        }
    }
}
