-- CREATE DB pos_ccb
USE master;
DROP DATABASE IF EXISTS pos_ccb;
CREATE DATABASE pos_ccb;

GO

USE pos_ccb;

GO

--CREATE TABLE users
CREATE TABLE users (
	id INT IDENTITY(1,1) PRIMARY KEY,
	fullname VARCHAR(50) NOT NULL, 
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	CONSTRAINT UC_users UNIQUE (username)
);
GO

--INSERT default users
INSERT INTO users VALUES
('Superadmin', 'superadmin', 'posccb2022$$');

GO

--CREATE TABLE products
CREATE TABLE products (
	id INT IDENTITY(1,1) PRIMARY KEY,
	name VARCHAR(50) NOT NULL,
	description TEXT NULL,
	category CHAR(1) NOT NULL DEFAULT 'F',
	filename VARCHAR(255) NULL,
	price FLOAT(1) NOT NULL
);

GO

--INSERT TABLE products
INSERT INTO products VALUES
('JJANGMYEON', '', 'F', 'Food-1.png', 20500),
('TTEOBOKKI', '', 'F', 'Food-1.png', 19000),
('RABBOKKI', '', 'F', 'Food-1.png', 20000),
('RAMEN ORIGINAL', '', 'F', 'Food-1.png', 18000),
('RAMEN CURRY', '', 'F', 'Food-1.png', 21000),
('AVOCADO JUICE', '', 'B', 'Beverage-1.png', 15000),
('ICED TEA', '', 'B', 'Beverage-1.png', 6500),
('WHITE CHOCOLATE', '', 'B', 'Beverage-1.png', 17500);

GO

--CREATE TABLE carts
CREATE TABLE carts (
	id INT IDENTITY(1,1) PRIMARY KEY,
	product_id INT,
	qty INT,
	FOREIGN KEY (product_id) REFERENCES products(id)
);

GO

--INSERT TABLE carts
INSERT INTO carts VALUES
(2, 2),
(6, 1);

GO

--CREATE TABLE orders
CREATE TABLE orders (
	id INT IDENTITY(1,1) PRIMARY KEY,
	customer_name VARCHAR(50) NULL,
	transaction_time DATETIME DEFAULT CURRENT_TIMESTAMP,
	grand_total FLOAT(1) NOT NULL,
	payment FLOAT(1) NOT NULL,
	change FLOAT(1) NOT NULL,
);

GO

--INSERT TABLE orders
INSERT INTO orders (customer_name, grand_total, payment, change) VALUES
('Dony P', 56000, 60000, 4000);

GO

--CREATE TABLE order_details
CREATE TABLE order_details (
	id INT IDENTITY(1,1) PRIMARY KEY,
	order_id INT,
	product_id INT,
	price FLOAT(1) NOT NULL,
	qty INT NOT NULL,
	total_price FLOAT(1) NOT NULL,
	FOREIGN KEY (order_id) REFERENCES orders(id),
	FOREIGN KEY (product_id) REFERENCES products(id)
);

GO

--INSERT TABLE order_details
INSERT INTO order_details VALUES
(1, 1, 20500, 2, 41000),
(1, 6, 15000, 1, 15000);

GO

SELECT * FROM users;
SELECT * FROM products;
SELECT * FROM carts;
SELECT * FROM orders;
SELECT * FROM order_details;